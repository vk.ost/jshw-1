"use strict";

// 1) Змінну можно оголосити через оператори let i const, 
// а також  через оператор var, але це застарілий варіант.

// 2) Функція prompt повертае будь-яке значення введенне користовачем,
// а функція confirm повертае лише true або false, в залежності від того, що вибрав користувач.

// 3) Це перетворення типів не використовюючи існуючих для цього функцій, 
// а використовування логічний або арефметичних операторів або унарного плюсу. let str = '3'; +str;


let name = "Viktor";
let admin;
admin = name;
console.log(admin);

const days = Math.floor(Math.random() * 10) + 1;
let secunds = days*24*60*60; // or let secunds = days*86400;
if(days === 1){
    console.log(`В ${days} днi ${secunds} секунд.`);
}else{
    console.log(`В ${days} днях ${secunds} секунд.`);
}

const answear = prompt("Чи сподобалась Вам робота?");
console.log(answear);